from django.contrib.auth.models import User
from django.db import models


class Doctor(models.Model):
    type = models.CharField(max_length=32)
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)

    def __str__(self):
        return f"{self.type}: {self.first_name} {self.last_name}"


class DoctorVisit(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.PROTECT)
    reason = models.TextField()
    data_time = models.DateTimeField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=1, default="Ожидает", choices=(('w', "Ожидает"), ("c", "Одобрено")))

    class Meta:
        ordering = ("-data_time",)

# * Модель запроса на поход к доктору
# 	* *Особые поля доктора
# * Дашборд со всеми запросами
# * Форма нового запроса
