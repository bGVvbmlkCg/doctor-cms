from django.contrib import admin

from .models import Doctor, DoctorVisit


@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    list_display = ('type', 'first_name', 'last_name')
    list_editable = ('first_name', 'last_name')
    list_display_links = ('type',)


@admin.register(DoctorVisit)
class DoctorAdmin(admin.ModelAdmin):
    list_display = ('doctor', 'user', 'data_time', 'status')
    list_editable = ('status',)
